" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Start a :e with the dir of the current buffer already filled in
nnoremap ,e :e <C-R>=Get_Relative_Cwd() <CR>
